﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiDisplay : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(gameObject.name == "Paint"){
         if(PlayerPrefs.GetString("PaintColour")!=null)
        GetComponent<Renderer>().material.color  = ColorMapper.StringToColor(PlayerPrefs.GetString("PaintColour"));
         }else if(gameObject.name == "Plane"){
             if(PlayerPrefs.GetString("PlaneColour")!=null)
        GetComponent<Renderer>().material.color  = ColorMapper.StringToColor(PlayerPrefs.GetString("PlaneColour"));
         }
    
    }

    // Update is called once per frame
    void Update()
    {
        if(gameObject.name == "Paint"){
        GetComponent<Renderer>().material.color  = ColorMapper.StringToColor(PlayerPrefs.GetString("PaintColour"));
         }else if(gameObject.name == "Plane"){
        GetComponent<Renderer>().material.color  = ColorMapper.StringToColor(PlayerPrefs.GetString("PlaneColour"));
         }
         transform.Rotate(0,1,0);

    }
}
