﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    
    public void LoadLevel(string sceneName){
        Debug.Log("Button Clicked");
        SceneManager.LoadScene(sceneName);
        
    }
    public void Exit(){
        Application.Quit();    }
}
