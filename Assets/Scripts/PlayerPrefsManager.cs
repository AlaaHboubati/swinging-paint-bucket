﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsManager : MonoBehaviour
{
    void Awake()
    {
        if (!PlayerPrefs.HasKey("RopeLength")) PlayerPrefs.SetFloat("RopeLength",4f);
        if (!PlayerPrefs.HasKey("InitialTheta")) PlayerPrefs.SetFloat("InitialTheta",1f);
        if (!PlayerPrefs.HasKey("InitialAlpha")) PlayerPrefs.SetFloat("InitialAlpha",0f);
        if (!PlayerPrefs.HasKey("InitialThetaD")) PlayerPrefs.SetFloat("InitialThetaD",0.4f);
        if (!PlayerPrefs.HasKey("InitialAlphaD")) PlayerPrefs.SetFloat("InitialAlphaD",0.4f);
        if (!PlayerPrefs.HasKey("Mui")) PlayerPrefs.SetFloat("Mui",0.03f);
        if (!PlayerPrefs.HasKey("Holes")) PlayerPrefs.SetFloat("Holes",1f);
        if (!PlayerPrefs.HasKey("Height")) PlayerPrefs.SetFloat("Height",10);
        if (!PlayerPrefs.HasKey("PaintColour"))PlayerPrefs.SetString("PaintColour","Red");
        if (!PlayerPrefs.HasKey("PlaneColour"))PlayerPrefs.SetString("PlaneColour","White");
       
    }
}
