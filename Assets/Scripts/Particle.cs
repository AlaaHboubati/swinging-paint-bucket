﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour
{   
    public Vector3 position;
    public float mass;
    public Vector3 velocity;
    public Vector3 accleration;

    public Vector3 totalForce = new Vector3(0,-9.8f,0);


    public PaintRenderer paintRenderer;
    public ParticleSpawner particleSpawner;
    public static float h;
    private float initVelo;

    private float t;
    private float y0;



    void Start(){
        position = transform.position;
        y0 = position.y;
        initVelo = -Mathf.Sqrt(2f*9.8f*h);
       
    }

    void Update(){
        position.y = initVelo*t + y0 - (1/2*9.8f*t*t);
        transform.position = position;
        if(position.y <= -10.9){
          position.y = -10.9f;
          transform.position = position;
          paintRenderer.Draw(transform.position);
          particleSpawner.Notify(gameObject);
        }
        t+=Time.deltaTime;
      
    }



    
   
}
