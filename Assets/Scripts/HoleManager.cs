﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleManager : MonoBehaviour
{
    public GameObject particleSpawnerPrefab;
    public GameObject paintRendererPrefab;

    public GameObject fallingRendererPrefab;

    public GameObject particleRendererPrefab;

    public GameObject [] holes;
    public int size;
    void Awake(){
        size = (int)PlayerPrefs.GetFloat("Holes");
        for(int i=0; i < size; i++){
            ParticleSpawner particleSpawner = Instantiate(particleSpawnerPrefab).GetComponent<ParticleSpawner>();
            PaintRenderer paintRenderer = Instantiate(paintRendererPrefab).GetComponent<PaintRenderer>();
            LineRenderer fallingRenderer = Instantiate(fallingRendererPrefab).GetComponent<LineRenderer>();
            LineRenderer particleRenderer = Instantiate(particleRendererPrefab).GetComponent<LineRenderer>();
            particleSpawner.paintRenderer = paintRenderer;
            particleSpawner.lineRenderer = fallingRenderer;
            particleSpawner.hole = holes[i];
            paintRenderer.lineRenderer = particleRenderer;
        }
    }
   
}
