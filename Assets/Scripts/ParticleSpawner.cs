﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ParticleSpawner : MonoBehaviour
{
    public GameObject particlePrefab;
    List<GameObject> particles = new List<GameObject>();

    public GameObject hole;
    public PaintRenderer paintRenderer;
    public LineRenderer lineRenderer;

    private static float h = 10;

    void Start()
    {
      
        h = PlayerPrefs.GetFloat("Height");
        lineRenderer.material.color = ColorMapper.StringToColor(PlayerPrefs.GetString("PaintColour"));



        
    }

    // Update is called once per frame
    void Update()
    {
        lineRenderer.positionCount = 0;
        float val = Time.deltaTime*0.3f;
        h-=val;
        Particle.h = h;
        if(h < 10)
            return;
            
        GameObject particle = Instantiate(particlePrefab,hole.transform.position,Quaternion.identity);
        particles.Add(particle);
        particle.GetComponent<Particle>().particleSpawner = this;
        particle.GetComponent<Particle>().paintRenderer = paintRenderer;

         for(int i=0; i < particles.Count; i++){
            GameObject pi = particles.ElementAt(i);
            lineRenderer.positionCount++;
            lineRenderer.SetPosition(i,pi.transform.position);
        }
       
        
    }

    public void Notify(GameObject particle){
            particles.Remove(particle);
            Destroy(particle);
    }
}
