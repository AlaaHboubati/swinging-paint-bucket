﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintRenderer : MonoBehaviour
{
    public LineRenderer  lineRenderer;
    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.material.color = ColorMapper.StringToColor(PlayerPrefs.GetString("PaintColour"));
        
    }

    // Update is called once per frame
    public void Draw(Vector3 pos){
        int count = lineRenderer.positionCount;
        lineRenderer.positionCount+=1;
        lineRenderer.SetPosition(count,pos);
    }
}
