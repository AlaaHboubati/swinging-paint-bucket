﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class PendulumController : MonoBehaviour
{

    public GameObject line;
    private LineRenderer lineRenderer;
    private Vector3 pos;

    public float theta;
    public float alpha;

    public float mui;

    public float theta0 = Mathf.PI/3;
    public float alpha0;
    public float theta_d0;
    public float alpha_d0;

    private Vector4 curState;

    private GameObject grip;
    
    
    public float l;
    public  float g = 9.8f;
    
    Vector4 G(Vector4 prevState)
    {
        float theta_d, alpha_d, theta, alpha;
        float theta_dd, alpha_dd;

        theta_d = prevState.x;
        alpha_d = prevState.y;
        theta = prevState.z;
        alpha = prevState.w; 
        
        theta_dd = (alpha_d * alpha_d) * Mathf.Cos(theta) * Mathf.Sin(theta) - (g / l) * Mathf.Sin(theta)-mui*theta_d;
        alpha_dd = (-2f * theta_d * alpha_d )/ Mathf.Tan(theta)-mui*alpha_d;

        Vector4 curState = new Vector4(theta_dd,alpha_dd,theta_d,alpha_d);
       
        return curState;
    }
    
    

    Vector4 RK4_step(Vector4 prevState)
    {
        Vector4 k1 = G(prevState);
        Vector4 k2 = G(prevState + 0.5f * Time.deltaTime * k1);
        Vector4 k3 = G(prevState + 0.5f * Time.deltaTime * k2);
        Vector4 k4 = G(prevState + k3 * Time.deltaTime);

        return Time.deltaTime * (k1 + 2f * k2 + 2f * k3 + k4) / 6f;
    }
    
    
    void Start()
    {
        
        theta0 = PlayerPrefs.GetFloat("InitialTheta");
        alpha0 = PlayerPrefs.GetFloat("InitialAlpha");
        theta_d0 = PlayerPrefs.GetFloat("InitialThetaD");
        alpha_d0 = PlayerPrefs.GetFloat("InitialAlphaD");
        l = PlayerPrefs.GetFloat("RopeLength");
        mui = PlayerPrefs.GetFloat("Mui");
        grip = GameObject.Find("Grip");
        


        curState = new Vector4(theta_d0, alpha_d0, theta0, alpha0);
        Vector3 pos = Polar2Cartesian(theta0, alpha0);
        transform.position = pos;
        lineRenderer = line.GetComponent<LineRenderer>();

    }

    
    void Update()
    {

        if(Mathf.Abs(theta0) < 0.001){
            return ;
        }
        curState = curState + RK4_step(curState);
        theta = curState.z;
        alpha = curState.w;

        Vector3 pos = Polar2Cartesian(theta, alpha);
        transform.position = pos;
        transform.rotation = Quaternion.FromToRotation(-Vector3.up,transform.position-new Vector3(0.0f,0.0f,0.0f));
        lineRenderer.SetPosition(1,grip.transform.position);

        //Instantiate(Paint, new Vector3(pos.x, -40, pos.z), Quaternion.identity);

    }


    // to convert theta and alpha to x,y,z
    
    Vector3  Polar2Cartesian(float theta,float alpha)
    {
        
        float x = l * Mathf.Sin(theta) * Mathf.Cos(alpha);
        float y = -l * Mathf.Cos(theta);
        float z = l * Mathf.Sin(theta) * Mathf.Sin(alpha);
        Vector3 pos = new Vector3(x, y, z);
        return pos;
    }


 
}





