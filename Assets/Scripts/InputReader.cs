﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputReader : MonoBehaviour
{

    GameObject myValue;
    
    public void Awake(){
        myValue = (transform.GetChild(3)).GetChild(0).gameObject;
        gameObject.GetComponent<Slider>().value = PlayerPrefs.GetFloat(gameObject.name);
        myValue.GetComponent<Text>().text = PlayerPrefs.GetFloat(gameObject.name).ToString();

    }
    public void OnValueChange(float value){
        PlayerPrefs.SetFloat(gameObject.name,value);
        myValue.GetComponent<Text>().text = value.ToString();
    }
    

    

}
