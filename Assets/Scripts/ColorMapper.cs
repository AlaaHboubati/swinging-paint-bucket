﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorMapper : MonoBehaviour
{
   public static Color StringToColor(string colorName){
       if(colorName.Equals("Red")){
           return Color.red;
       }else if(colorName.Equals("Green")){
           return Color.green;
       }else if(colorName.Equals("Blue")){
           return Color.blue;
       }else if(colorName.Equals("Yellow")){
           return Color.yellow;
       }else if(colorName.Equals("Magenta")){
           return Color.magenta;
       }else {
           return Color.white;
       }
       
   }
}
