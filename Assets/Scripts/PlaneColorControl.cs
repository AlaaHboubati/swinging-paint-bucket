﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneColorControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().material.color  = ColorMapper.StringToColor(PlayerPrefs.GetString("PlaneColour"));

    }

    // Update is called once per frame
    
}
